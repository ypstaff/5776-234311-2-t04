// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PlayerData.h"
#include "HashMap.h"
#include "Object.h"
#include "GameDataSerialization.generated.h"


/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API UGameDataSerialization : public UObject
{
	GENERATED_BODY()

		TSharedPtr<FJsonObject> gameData;
		
	
public:
	UGameDataSerialization();

	// interactive states serialization
	/*
	* sets the game data variable to hold the state of all interactive objects in the game.
	* param:
	* interactiveStates - an array of all interactive states holding true if doAction is on, and false otherwise.
	*/
	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	void setInteractiveStates(TArray<bool> interactiveStates);

	/*
	* gets the interactiveStates array.
	*/
	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	TArray<bool> getInteractiveStates();

	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	void setMapData(TArray<FVector> mapData);

	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	TArray<FVector> getMapData();
	
	// scoreboard serialization.
	/*
	* sets the game data variable to hold the Name -> Score of the players.
	* param:
	* scoreboard - a hasing of FString -> PlayerData holding the name -> score of the players.
	*/
	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	void setNameToScoreMap(UHashMap* scoreboard);

	/*
	* gets the game data variables holding the name-> score of players.
	*/
	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	UHashMap* getNameToScoreMap();


	// serialization functions.
	// serializes all information in the GameDataSerialization.
	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	FString serialize();

	// deserializes all information in the GameDataSerialization.
	UFUNCTION(BlueprintCallable, Category = "GameDataSerialization")
	void deserialize(FString str);
};
