// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "SessionResult.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API USessionResult : public UObject
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SessionResult")
	int32 CurrentPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SessionResult")
	int32 MaxPlayers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SessionResult")
	int32 PingInMs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SessionResult")
	FName ServerName;

	int32 ResultIndex;


	USessionResult();
	
};
