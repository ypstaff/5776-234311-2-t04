// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "ObjectHashMap.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API UObjectHashMap : public UObject
{
	GENERATED_BODY()
	
		TMap<UObject*, TArray<float> > objectHashMap;
	
public:

	UFUNCTION(BlueprintCallable, Category = "ObjectHashMap")
	void Add(UObject* key, float element);

	UFUNCTION(BlueprintCallable, Category = "ObjectHashMap")
	void SetArray(UObject* key, TArray<float> arr);

	UFUNCTION(BlueprintCallable, Category = "ObjectHashMap")
		TArray<float>& GetArray(UObject* key);

	UFUNCTION(BlueprintCallable, Category = "ObjectHashMap")
	void Clear(UObject* key);

	UFUNCTION(BlueprintCallable, Category = "ObjectHashMap")
	void Empty();
};
