// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "StringWrapper.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API UStringWrapper : public UObject
{
	GENERATED_BODY()
	
	FString string;
	
public:

	UFUNCTION(BlueprintCallable, Category = "StringWrapper")
	FString GetString();

	UFUNCTION(BlueprintCallable, Category = "StringWrapper")
	void SetString(FString str);
	
};
