// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "ArrayMap.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API UArrayMap : public UObject
{
	GENERATED_BODY()

		TMap<int32, TArray<UObject*> > arrayMap;
	
public:

	UFUNCTION(BlueprintCallable, Category = "ArrayMap")
	void Add(int32 key, UObject* element);

	UFUNCTION(BlueprintCallable, Category = "ArrayMap")
	void SetArray(int32 key, TArray<UObject*> arr);

	UFUNCTION(BlueprintCallable, Category = "ArrayMap")
	TArray<UObject*>& GetArray(int32 key);

	UFUNCTION(BlueprintCallable, Category = "ArrayMap")
	void Clear(int32 key);
	
};
