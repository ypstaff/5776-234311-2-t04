// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "SpectatorManager.generated.h"

/**
 * Manages transfer to spectator mode and to player mode for a given PlayerController.
 */
UCLASS(BlueprintType)
class LASERDEMO_API USpectatorManager : public UObject
{
	GENERATED_BODY()
	
public:

	/*
	* Transfers a given PlayerController to spectator mode.
	* param:
	* PlayerController - the PlayerController to transfer.
	*/
	UFUNCTION(BlueprintCallable, Category = "SpectatorManager")
		void ChangeStateToSpectator(APlayerController* PlayerController, bool IsSpectatorTop);

	/*
	* Transfers a given PlayerController to player mode.
	* param:
	* PlayerController - the PlayerController to transfer.
	*/
	UFUNCTION(BlueprintCallable, Category = "SpectatorManager")
		void ChangeStateToPlayer(APlayerController* PlayerController);
	
};
