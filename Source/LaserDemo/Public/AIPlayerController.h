// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "AIPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LASERDEMO_API AAIPlayerController : public AAIController
{
	GENERATED_BODY()
	
	AAIPlayerController(const FObjectInitializer& ObjectInitializer);
	
};
