// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "HashMap.generated.h"

/**
 * holds a hashing of strings to objects - the order of the objects in the memory can NOT be relied on.
 */
UCLASS(BlueprintType)
class LASERDEMO_API UHashMap : public UObject
{
	GENERATED_BODY()
		// Blueprint callable Objects cannot use references. must hold pointers.
		UPROPERTY()
		TMap<FString, UObject*> map;

public:

	/*
	* Gets the value in the HashMap under the key index.
	* param:
	* key - the index to take the object from.
	*/
	UFUNCTION(BlueprintCallable, Category = "HashMap")
	UObject* Get(FString key);

	/*
	* sets the value of a key.
	* param:
	* key - the index of the object to place.
	* newValue - the new object to place in the key.
	*/
	UFUNCTION(BlueprintCallable, Category = "HashMap")
	void Set(FString key, UObject* newValue);

	/*
	* Deletes the key-value pair from the HashMap
	* param:
	* key - the index to delete.
	*/
	UFUNCTION(BlueprintCallable, Category = "HashMap")
	void Remove(FString key);

	/*
	* Deletes all the data in the HashMAp - THE ALLOCATED MEMORY IS NOT DEALLOCATED!!
	*/
	UFUNCTION(BlueprintCallable, Category = "HashMap")
	void Clear();

	/*
	* Returns all the keys in the map.
	*/
	UFUNCTION(BlueprintCallable, Category = "HashMap")
	TArray<FString> GetKeys();

	/*
	* Returns all the keys in the map.
	*/
	UFUNCTION(BlueprintCallable, Category = "HashMap")
	bool Contains(FString key);
};
