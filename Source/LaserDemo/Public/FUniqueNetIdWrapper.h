// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "FUniqueNetIdWrapper.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API UFUniqueNetIdWrapper : public UObject
{
	GENERATED_BODY()

public:

	FUniqueNetIdString SessionId;

	UFUniqueNetIdWrapper();

	bool UFUniqueNetIdWrapper::operator==(const UFUniqueNetIdWrapper& other);

	UFUNCTION(BlueprintCallable, Category = "Network")
	FString ToString();

	UFUNCTION(BlueprintCallable, Category = "Network")
	void SetIdWrapper(APlayerState* PlayerState);
};
