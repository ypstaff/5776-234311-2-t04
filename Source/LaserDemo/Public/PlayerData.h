// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "PlayerData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LASERDEMO_API UPlayerData : public UObject
{
	GENERATED_BODY()

	UPROPERTY()
	FString PlayerName;

	UPROPERTY()
	int32 PlayerScore;

	UPROPERTY()
	int32 ScoreboardRowIndex;

public:

	/*
	Configure the entire player data
	param:
	name - the player name
	score - the player score
	rowIndex - the row index
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	void ConfigurePlayerData(FString name, int32 score, int32 rowIndex);


	/*
	* Returns the score of the player.
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	int32 GetPlayerScore();

	/*
	* Returns the name of the player.
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	FString GetPlayerName();

	/*
	* Returns the index of the row that the player score is in
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	int32 GetScoreboardRowIndex();
	

	/*
	* Sets the score of the player
	* param:
	* score - the score of the player.
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	void SetPlayerScore(int32 score);

	/*
	* Sets the name of the player
	* param:
	* name - the name of the player.
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	void SetPlayerName(FString name);

	/*
	* Sets the score of the player
	* param:
	* rowIndex - the index of the row
	*/
	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	void SetScoreboardRowIndex(int32 rowIndex);

};
