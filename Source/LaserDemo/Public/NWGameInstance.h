// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SessionResult.h"
#include "FUniqueNetIdWrapper.h"
#include "Engine/GameInstance.h"
#include "NWGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBlueprintStartSessionResultDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBlueprintFindSessionsResultDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBlueprintFindFriendSessionsResultDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBlueprintMigrateToSessionResultDelegate);

/**
*
*/
UCLASS()
class LASERDEMO_API UNWGameInstance : public UGameInstance
{
	GENERATED_BODY()

	bool SearchStarted = false;
	bool FriendSearchStarted = false;
public:

	/*
	Variable Part for CREATING a Session
	*/

	FName CurrentSessionName = GameSessionName;

	TSharedPtr<class FOnlineSessionSettings> SessionSettings;

	/* Delegate called when session created */
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	/* Delegate called when session started */
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

	/** Handles to registered delegates for creating a session */
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;

	/*
	Variable Part for FINDING a Session
	*/

	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	/** Delegate for searching for sessions */
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

	/** Handle to registered delegate for searching a session */
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;

	/*
	Variable Part for JOINING a Session
	*/

	/** Delegate after joining a session */
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

	/** Handle to registered delegate for joining a session */
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;

	/*
	Variable Part for DESTROYING a Session
	*/

	/** Delegate for destroying a session */
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

	/** Handle to registered delegate for destroying a session */
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;


	FOnFindSessionsCompleteDelegate OnFindFriendSessionCompleteDelegate;

	FDelegateHandle OnFindFriendSessionCompleteDelegateHandle;


	FOnlineSessionSearchResult SingleSearchResult;


	FString FriendIdStr;

	// Called when there is a successful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintStartSessionResultDelegate OnStartSessionSuccess;

	// Called when there is an unsuccessful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintStartSessionResultDelegate OnStartSessionFailure;


	// Called when there is a successful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintFindSessionsResultDelegate OnFindSuccess;

	// Called when there is an unsuccessful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintFindSessionsResultDelegate OnFindFailure;

	// Called when there is a successful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintFindFriendSessionsResultDelegate OnFindFriendSuccess;

	// Called when there is an unsuccessful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintFindFriendSessionsResultDelegate OnFindFriendFailure;

	// Called when there is a successful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintMigrateToSessionResultDelegate OnMigrateSuccess;

	// Called when there is an unsuccessful query
	UPROPERTY(BlueprintAssignable)
		FBlueprintMigrateToSessionResultDelegate OnMigrateFailure;

	/// Functions ///
public:

	// Constuctor

	UNWGameInstance(const FObjectInitializer& ObjectInitializer);

	/*
	Function Part for CREATING a Session
	*/

	/**
	*	Function to host a game!
	*
	*	@Param		UserID			User that started the request
	*	@Param		SessionName		Name of the Session
	*	@Param		bIsLAN			Is this is LAN Game?
	*	@Param		bIsPresence		"Is the Session to create a presence Session"
	*	@Param		MaxNumPlayers
	*/
	void HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);


	/**
	*	Delegate fired when a session create request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	virtual void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);

	/**
	*	Delegate fired when a session start request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful);

	// TEST BP FUNCTION

	UFUNCTION(BlueprintCallable, Category = "Network")
		void StartOnlineGame(FName SessionName, int32 NumConnections, bool bIsLan);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void ResetCurrentSessionName();


	/*
	Function part for FINDING a Session
	*/

	/**
	*	Find an online session
	*
	*	@param UserId user that initiated the request
	*	@param SessionName name of session this search will generate
	*	@param bIsLAN are we searching LAN matches
	*	@param bIsPresence are we searching presence sessions
	*/
	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence);

	/**
	*	Delegate fired when a session search query has completed
	*
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnFindSessionsComplete(bool bWasSuccessful);

	// TEST BP FUNCTION

	UFUNCTION(BlueprintCallable, Category = "Network")
		void FindOnlineGames(bool bIsLAN);

	UFUNCTION(BlueprintCallable, Category = "Network")
		TArray< USessionResult* > GetSearchResult();

	UFUNCTION(BlueprintCallable, Category = "Network")
		bool IsSearching();

	/*
	Function part for JOINING a Session
	*/

	/**
	*	Joins a session via a search result
	*
	*	@param SessionName name of session
	*	@param SearchResult Session to join
	*
	*	@return bool true if successful, false otherwise
	*/
	bool JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);

	/**
	*	Delegate fired when a session join request has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	// TEST BP FUNCTION

	UFUNCTION(BlueprintCallable, Category = "Network")
		bool JoinOnlineGame(USessionResult* SessionResult);

	/*
	Function part for DESTROYING a Session
	*/

	/**
	*	Delegate fired when a destroying an online session has completed
	*
	*	@param SessionName the name of the session this callback is for
	*	@param bWasSuccessful true if the async action completed without error, false if there was an error
	*/
	void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	bool DestroySession();

	// TEST BP FUNCTION

	UFUNCTION(BlueprintCallable, Category = "Network")
		bool DestroySessionAndLeaveGame();


	void OnFindFriendSessionComplete(bool bWasSuccessful);

	void FindSessionById(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void FindOnlineGameById(FString IdStr, bool bIsLAN);

	UFUNCTION(BlueprintCallable, Category = "Network")
		FString GetIdStr(APlayerState* PlayerState);

	UFUNCTION(BlueprintCallable, Category = "Network")
		void MigrateToSession();
};
