#pragma once

#include <fstream>
#include "BehaviorTree/Tasks/BTTask_BlueprintBase.h"
#include "AIController.h"
#include "CustomAiBlueprintBTT.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBlueprintShootHoldDelegate);

UCLASS()
class LASERDEMO_API UCustomAiBlueprintBTT : public UBTTask_BlueprintBase
{
	GENERATED_BODY()

	int	callNumber = 0;
	int orderNumber = 0;
	int operationMode = 1;
	int shootTime = -3;
	//the file of orders - needs to be placed in the folder of the game
	std::ifstream ordersFile;

public:
	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void DoStuff(AAIController* OwnerActor, TArray<AActor*> Enemies);

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void walkByFileorders(AAIController* OwnerActor);

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void shoot(AAIController* OwnerActor);

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void chooseOperationMode(AAIController* OwnerActor, TArray<AActor*> Enemies);

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	bool openOrdersFile();

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void goToPoint(AAIController* OwnerActor);

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void followActor(AAIController* OwnerActor, TArray<AActor*> Enemies);

	UFUNCTION(BlueprintCallable, Category = "CusomAI")
	void walkByScreensaver(AAIController* OwnerActor);
	//UPROPERTY(BlueprintAssignable)
		//FBlueprintShootHoldDelegate OnShootHold;
	/*void CallBPEvent()
	{
		this->ExecuteAbility(C);
	}*/
};
