// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "SpectatorManager.h"


void USpectatorManager::ChangeStateToSpectator(APlayerController* PlayerController, bool IsSpectatorTop) {
	PlayerController->ChangeState(NAME_Spectating);
	if (PlayerController->Role != ROLE_Authority){
		PlayerController->PlayerState->bIsSpectator = true;
		if (IsSpectatorTop){
			PlayerController->GetSpectatorPawn()->SetActorLocation(FVector(1000.0, 1000.0, 700.0));
			PlayerController->GetSpectatorPawn()->GetMovementComponent()->SetPlaneConstraintNormal(FVector(0, 0, 1));
			PlayerController->GetSpectatorPawn()->GetMovementComponent()->bConstrainToPlane = true;
		}
	}
}

void USpectatorManager::ChangeStateToPlayer(APlayerController* PlayerController) {
	PlayerController->ChangeState(NAME_Playing);
	if (PlayerController->Role != ROLE_Authority){
		PlayerController->PlayerState->bIsSpectator = false;
	}
}

