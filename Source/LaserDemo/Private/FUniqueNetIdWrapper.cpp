// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "FUniqueNetIdWrapper.h"



UFUniqueNetIdWrapper::UFUniqueNetIdWrapper() {}

bool UFUniqueNetIdWrapper::operator==(const UFUniqueNetIdWrapper& other)
{
	return SessionId.UniqueNetIdStr.Equals(other.SessionId.UniqueNetIdStr);
}

FString UFUniqueNetIdWrapper::ToString()
{
	return SessionId.UniqueNetIdStr;
}

void UFUniqueNetIdWrapper::SetIdWrapper(APlayerState* PlayerState)
{
	SessionId.UniqueNetIdStr = static_cast<FUniqueNetIdString>(*(PlayerState->UniqueId.GetUniqueNetId())).UniqueNetIdStr;
}