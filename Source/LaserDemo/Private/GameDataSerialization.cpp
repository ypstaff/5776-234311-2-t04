// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "GameDataSerialization.h"

UGameDataSerialization::UGameDataSerialization() {
	gameData = MakeShareable(new FJsonObject);
}

void UGameDataSerialization::setInteractiveStates(TArray<bool> interactiveStates) {
	TArray<TSharedPtr<FJsonValue>> arr;
	for (int i = 0; i < interactiveStates.Num(); i++) {
		arr.Add(MakeShareable(new FJsonValueBoolean(interactiveStates[i])));
	}

	gameData->SetArrayField("InteractiveStates", arr);
}

TArray<bool> UGameDataSerialization::getInteractiveStates() {
	TArray<TSharedPtr<FJsonValue>> arr = gameData->GetArrayField("InteractiveStates");
	TArray<bool> interactiveStates;
	for (int i = 0; i < arr.Num(); i++) {
		bool temp;
		arr[i]->TryGetBool(temp);	// array of bool.
		interactiveStates.Add(temp);
	}
	return interactiveStates;
}

void UGameDataSerialization::setMapData(TArray<FVector> mapData) {
	TArray<TSharedPtr<FJsonValue>> arrx;
	TArray<TSharedPtr<FJsonValue>> arry;
	TArray<TSharedPtr<FJsonValue>> arrz;

	for (int i = 0; i < mapData.Num(); i++) {
		arrx.Add(MakeShareable(new FJsonValueNumber(mapData[i].X)));
		arry.Add(MakeShareable(new FJsonValueNumber(mapData[i].Y)));
		arrz.Add(MakeShareable(new FJsonValueNumber(mapData[i].Z)));
	}

	gameData->SetArrayField("MapDataX", arrx);
	gameData->SetArrayField("MapDataY", arry);
	gameData->SetArrayField("MapDataZ", arrz);
}

TArray<FVector> UGameDataSerialization::getMapData() {
	TArray<FVector> mapData;
	TArray<TSharedPtr<FJsonValue>> arrx = gameData->GetArrayField("MapDataX");
	TArray<TSharedPtr<FJsonValue>> arry = gameData->GetArrayField("MapDataY");
	TArray<TSharedPtr<FJsonValue>> arrz = gameData->GetArrayField("MapDataZ");

	for (int i = 0; i < arrx.Num(); i++) {
		FVector v;
		double temp;
		arrx[i]->TryGetNumber(temp);	// arrays of int.
		v.X = (float)temp;
		arry[i]->TryGetNumber(temp);
		v.Y = (float)temp;
		arrz[i]->TryGetNumber(temp);
		v.Z = (float)temp;
		mapData.Add(v);
	}

	return mapData;
}

void UGameDataSerialization::setNameToScoreMap(UHashMap* indexToNameMap) {
	// the hashmap given is FString -> UPlayerData
	TArray<TSharedPtr<FJsonValue>> playerScores;
	TArray<TSharedPtr<FJsonValue>> playerNames;
	for (FString key : indexToNameMap->GetKeys()) {
		// the name of the player.
		playerNames.Add(MakeShareable(new FJsonValueString(key)));
		// the score of a player from the player data object that corresponds to it.
		playerScores.Add(MakeShareable(new FJsonValueNumber(((UPlayerData*)indexToNameMap->Get(key))->GetPlayerScore())));
	}
	gameData->SetArrayField("playerScores", playerScores);
	gameData->SetArrayField("playerNames", playerNames);
}

UHashMap* UGameDataSerialization::getNameToScoreMap() {
	TArray<TSharedPtr<FJsonValue>> playerScores = gameData->GetArrayField("playerScores");
	TArray<TSharedPtr<FJsonValue>> playerNames = gameData->GetArrayField("playerNames");
	UHashMap* nameToScoreMap = ConstructObject<UHashMap>(UHashMap::StaticClass());
	UPlayerData* data;
	//UPlayerData* data = ConstructObject<UPlayerData>(UPlayerData::StaticClass());	// hashMap only works for objects inheriting UObject - so cannot return a hashing of ints.
	// loop temp variables to take data from the json.
	FString playerName;
	int playerScore;
	// taking the hash values from the serialized arrays.
	for (int i = 0; i < playerNames.Num(); i++) {
		data = ConstructObject<UPlayerData>(UPlayerData::StaticClass());
		playerNames[i]->TryGetString(playerName);	// get the names of the players from the jsonvalue holding them.
		playerScores[i]->TryGetNumber(playerScore);	// get the names of the players from the jsonvalue holding them.
		data->SetPlayerName(playerName);
		data->SetPlayerScore(playerScore);
		nameToScoreMap->Set(playerName, data);
	}
	return nameToScoreMap;
}

FString UGameDataSerialization::serialize() {
	FString outputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&outputString);
	FJsonSerializer::Serialize(gameData.ToSharedRef(), Writer);
	return outputString;
}

void UGameDataSerialization::deserialize(FString str) {
	TSharedRef< TJsonReader<TCHAR> > Reader = TJsonReaderFactory<TCHAR>::Create(str);
	FJsonSerializer::Deserialize(Reader, gameData);
}

