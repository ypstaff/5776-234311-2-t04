#include "LaserDemo.h"
#include "CustomAiBlueprintBTT.h"
#include <string>
#include <fstream>

/*// Fill out your copyright notice in the Description page of Project Settings.  
void UCustomAiBlueprintBTT::RotateToPoint(AAIController* OwnerActor, FVector& rotTo) {  
FVector PointLocation = rotTo;  
FVector DirectionalVector;
DirectionalVector = PointLocation - OwnerActor->GetPawn()->GetActorLocation(); 
DirectionalVector.Normalize(); 
DirectionalVector.X = 0;
FTransform PawnTransform = OwnerActor->GetPawn()->GetTransform(); 
FQuat PawnQuat = PawnTransform.GetRotation(); 
FVector PawnUpVector = PawnQuat.RotateVector(FVector(0, 0, 1));
FVector c = FVector::CrossProduct(PawnUpVector, DirectionalVector); 
float d = FVector::DotProduct(PawnUpVector, DirectionalVector); 
float s = FMath::Sqrt((1.0 + d) * 2.0);  
float rs = 1.0 / s;
FQuat q(c.X * rs, c.Y * rs, c.Z * rs, s * 0.5);
q = q * PawnQuat;
q.Normalize();
PawnTransform.SetRotation(q);
OwnerActor->GetPawn()->SetActorRotation(PawnTransform.Rotator());
OwnerActor->SetControlRotation(PawnTransform.Rotator());
}
OwnerActor->GetPawn()->SetActorLocation(f2, false);
void  UCustomAiBlueprintBTT::MoveToPoint(AAIController* OwnerActor, FVector p) {
EPathFollowingRequestResult::Type result = OwnerActor->MoveToLocation(p, 10.0, false, true, true, true);}
void UCustomAiBlueprintBTT::AimToEnemy(AAIController* OwnerActor, AActor enemy){}
//To change rotation, use the following functions. you want to change Yaw in order to keep parallel to the floor
/*FRotator rotf(0,0,0);
OwnerActor->GetPawn()->SetActorRotation(rotf);
OwnerActor->SetControlRotation(rotf);
//OwnerActor->HoldShootEvent();
//if (f2.X-1000 < 300) {
//}
//UNavigationSystem::SimpleMoveToLocation(OwnerActor, f);
/*if (Enemies.Num() == 1) {
GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "debug msg");
}
//const FString command = FString::Printf(TEXT("Move To %s %f %f %f"), OwnerActor, 1000.0, 0.0, 0.0);
//FOutputDeviceNull ar;
//this->CallFunctionByNameWithArguments(*command, ar, NULL, true);
*/

using std::string;

//absolute value calculating
#define ABS(x) (((x) > 0) ? (x) : (-(x)))

//epsilon for walking in each iteration
#define WALK_DISTANCE 50.0

//axises
#define X_AXIS 0
#define Y_AXIS 1

//walking directions of the axises
#define POSITIVE_DIRECTION 1
#define NEGATIVE_DIRECTION -1

//modes of operation
#define MODE_SCREENSAVER 0
#define MODE_ORDERSFILE 1
#define MODE_GOTO 2
#define MODE_FOLLOW 3
#define MODE_SHOOT 4

//name of the orders file
#define ORDERS_FILE_NAME "goOrders.txt"

//a constant that symbolizes the mode should not be changed from screensaver
#define STEPS_DONT_CHANGE_MODE -10

//number of last places saved
#define HISTORY 5

//the number of calls to the main function - moved to the class in the h file
//int callNumber = 0;

//the direction to go in each axis
int direction[2] = { POSITIVE_DIRECTION, POSITIVE_DIRECTION };

//last positions in the axises - the number of last positions is HISTORY
float last[2][HISTORY] = { { 0 } };

//number of steps left for the order
int steps = 0;

//walking axis right now - starting with axis X
int goAxis = X_AXIS;

//the mode of ooperation - moved to the class in the h file
//int operationMode = MODE_ORDERSFILE;

//destination point
float dest_X = 0.0, dest_Y = 0.0, dest_Z = 0.0;

//actor to follow
int actorNumber = 1;

//time left to shoot - moved to the class in the h file
//int shootTime = -3;

//the file of orders - needs to be placed in the folder of the game - moved to the class in the h file
//std::ifstream ordersFile;

/*
* Converting a string to a FString.
*/
FString stringToFString(string str)
{
	return FString(str.c_str());
}

/*
* Printing a message to the screen.
* The message is for walking orders, mode change, errors and debugging.
*/
void printMessage(FColor color, FString message)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, color, message);
}

/*
* Making a string that symbolizes the waking direction.
*/
string getWalkingString(int axis)
{
	string sign = direction[axis] == 1 ? "" : "-";
	string axisName = axis == X_AXIS ? "X" : "Y";
	return sign + axisName;
}

/*
* Following an actor numbered actorNumber.
*/
void UCustomAiBlueprintBTT::followActor(AAIController* OwnerActor, TArray<AActor*> Enemies)
{
	//vector.X = 0;
	//vector.Y = 0;
	//vector.Z = 0;
	if (actorNumber < 0 || actorNumber >= Enemies.Num() || OwnerActor == Enemies[actorNumber])
	{
		printMessage(FColor::Green, FString("Following actor ") + FString::FromInt(actorNumber) + " doesn't exist");
		return;
	}
	//printMessage(FColor::Green, FString("Actors number: ") + FString::FromInt(Enemies.Num()));
	//printMessage(FColor::Green, FString("Following actor ") + FString::FromInt(actorNumber));
	EPathFollowingRequestResult::Type result = OwnerActor->MoveToActor(Enemies[actorNumber], 60.0f);// , false, false, false, true, FilterClass, true);
	//EPathFollowingRequestResult::Type result = OwnerActor->MoveToLocation(vector, 60.0f);// , false, false, false, true, FilterClass, true);
	if (result != EPathFollowingRequestResult::RequestSuccessful) //result is Failed or AlreadyAtGoal
	{
		//printMessage(FColor::Green, result == EPathFollowingRequestResult::AlreadyAtGoal ? "AT-GOAL" : "FAILED");
		operationMode = MODE_ORDERSFILE;
		//printMessage(FColor::Green, FString("Reached actor ") + FString::FromInt(dest_X) + "," + FString::FromInt(dest_Y) + "," + FString::FromInt(dest_Z));
		//direction[axis] *= -1;
		//failed = true;
	}
}

/*
* Going to the point saved in dest_X, dest_Y, dest_Z.
*/
void UCustomAiBlueprintBTT::goToPoint(AAIController* OwnerActor)
{
	//vector.X = 0;
	//vector.Y = 0;
	//vector.Z = 0;
	//printMessage(FColor::Green, FString("Actors number: ") + FString::FromInt(Enemies.Num()));
	//EPathFollowingRequestResult::Type result = OwnerActor->MoveToActor(Enemies[0], 60.0f);// , false, false, false, true, FilterClass, true);
	if (callNumber % 10 != 0)
	{
		return;
	}

	FVector vector = OwnerActor->GetPawn()->GetActorLocation();
	float X = vector.Component(X_AXIS);// = vector.X;
	float Y = vector.Component(Y_AXIS);// = vector.Y;

	//if ((axis == X_AXIS && last[X_AXIS] == vector.Component(X_AXIS)) || (axis == Y_AXIS  && last[Y_AXIS] == vector.Component(Y_AXIS)))
	//if ((axis == X_AXIS && last[X_AXIS] == X) || (axis == Y_AXIS  && last[Y_AXIS] == Y))
	/*if (last[X_AXIS] == X  && last[Y_AXIS] == Y)
	{
	printMessage(FColor::Green, FString("vector unchanged   ") + (axis == X_AXIS ? "X" : "Y"));
	direction[axis] *= -1;
	failed = true;
	last[X_AXIS] = 0;
	last[Y_AXIS] = 0;
	}*/

	int vectorUnchanched = 0;
	for (int i = 0; i < HISTORY; i++)
	{
		if (last[X_AXIS][i] == X || last[Y_AXIS][i] == Y)
		{
			//printMessage(FColor::Green, FString("changed @@@@@@@@@   ") + (axis == X_AXIS ? "X   " : "Y   ") + );
			vectorUnchanched++;
			//break;
		}
	}
	//printMessage(FColor::Green, FString::FromInt(vectorUnchanched) + "     changes number");
	if (vectorUnchanched >= HISTORY / 2)
	{
		printMessage(FColor::Yellow, FString(FString("vector unchanged@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   ") + FString::FromInt(X) + "," + FString::FromInt(Y)));
		operationMode = MODE_ORDERSFILE;
		dest_X += 1.0;
		dest_Y += 1.0;
		//direction[axis] *= -1;
		//failed = true;
	}
	//printMessage(FColor::Green, FString::FromInt(X) + "," + FString::FromInt(Y) + " ----- " + FString::FromInt(last[X_AXIS][HISTORY - 1]) + "," + FString::FromInt(last[Y_AXIS][HISTORY - 1]));
	for (int i = 0; i < HISTORY - 1; i++)
	{
		last[X_AXIS][i] = last[X_AXIS][i + 1];
		last[Y_AXIS][i] = last[Y_AXIS][i + 1];
	}
	last[X_AXIS][HISTORY - 1] = vector.Component(X_AXIS);
	last[Y_AXIS][HISTORY - 1] = vector.Component(Y_AXIS);
	FVector vectorDest(dest_X, dest_Y, dest_Z);
	printMessage(FColor::Green, FString("Going to point ") + FString::FromInt(dest_X) + "," + FString::FromInt(dest_Y) + "," + FString::FromInt(dest_Z));
	EPathFollowingRequestResult::Type result = OwnerActor->MoveToLocation(vectorDest, 60.0f);// , false, false, false, true, FilterClass, true);
	//OwnerActor->AIMoveTo();
	//printMessage(FColor::Green, FString("Going to point ") + FString::FromInt(dest_X) + "," + FString::FromInt(dest_Y));// +"," + FString::FromInt(dest_Z));
	if (result != EPathFollowingRequestResult::RequestSuccessful) //result is Failed or AlreadyAtGoal
	{
		//printMessage(FColor::Green, result == EPathFollowingRequestResult::AlreadyAtGoal ? "AT-GOAL" : "FAILED");
		operationMode = MODE_ORDERSFILE;
		printMessage(FColor::Green, FString("Reached point ") + FString::FromInt(dest_X) + "," + FString::FromInt(dest_Y) + "," + FString::FromInt(dest_Z));
		//direction[axis] *= -1;
		//failed = true;
	}
}

/*
* Making a step in the given direction.
* axis: 0-x or 1-y, meaning X_AXIS or Y_AXIS
* It first updates the place vector.
* Then it executes the step, using the function MoveToLocation.
* At last, it checks if the step succeeded. If not, it means the bot bounced an element, so it changes the direction.
* That is done by checking the history of the last HISTORY places the bot been.
* If at least HISTORY/2 of them are the same as now in the relevant axis, it means the bot did not managed to walk.
*/
void goDirect(AAIController* OwnerActor, FVector &vector, int axis)// , bool &failed)
{
	float X = vector.Component(X_AXIS);// = vector.X;
	float Y = vector.Component(Y_AXIS);// = vector.Y;

	//if ((axis == X_AXIS && last[X_AXIS] == vector.Component(X_AXIS)) || (axis == Y_AXIS  && last[Y_AXIS] == vector.Component(Y_AXIS)))
	//if ((axis == X_AXIS && last[X_AXIS] == X) || (axis == Y_AXIS  && last[Y_AXIS] == Y))
	/*if (last[X_AXIS] == X  && last[Y_AXIS] == Y)
	{
		printMessage(FColor::Green, FString("vector unchanged   ") + (axis == X_AXIS ? "X" : "Y"));
		direction[axis] *= -1;
		failed = true;
		last[X_AXIS] = 0;
		last[Y_AXIS] = 0;
	}*/

	int vectorUnchanched = 0;
	for (int i = 0; i < HISTORY; i++)
	{
		if ((axis == X_AXIS && last[X_AXIS][i] == X) || (axis == Y_AXIS  && last[Y_AXIS][i] == Y))
		{
			//printMessage(FColor::Green, FString("changed @@@@@@@@@   ") + (axis == X_AXIS ? "X   " : "Y   ") + );
			vectorUnchanched++;
			//break;
		}
	}
	//printMessage(FColor::Green, FString::FromInt(vectorUnchanched) + "     changes number");
	if (vectorUnchanched >= HISTORY / 2)
	{
		//printMessage(FColor::Green, FString("vector unchanged   ") + (axis == X_AXIS ? "X" : "Y"));
		direction[axis] *= -1;
		//failed = true;
	}
	//printMessage(FColor::Green, FString::FromInt(X) + "," + FString::FromInt(Y) + " ----- " + FString::FromInt(last[X_AXIS][HISTORY - 1]) + "," + FString::FromInt(last[Y_AXIS][HISTORY - 1]));
	for (int i = 0; i < HISTORY-1; i++)
	{
		last[X_AXIS][i] = last[X_AXIS][i+1];
		last[Y_AXIS][i] = last[Y_AXIS][i + 1];
	}
	last[X_AXIS][HISTORY - 1] = vector.Component(X_AXIS);
	last[Y_AXIS][HISTORY - 1] = vector.Component(Y_AXIS);

	if (axis == X_AXIS)
	{
		vector.X += WALK_DISTANCE * direction[axis];
	}
	else if (axis == Y_AXIS)
	{
		vector.Y += WALK_DISTANCE * direction[axis];
	}
	else
	{
		printMessage(FColor::Red, "ERROR: illegal walk axis");
		return;
	}

	EPathFollowingRequestResult::Type result = OwnerActor->MoveToLocation(vector, 0.0f, false, false, false, true);// , FilterClass, true);
	//EPathFollowingRequestResult::Type result = OwnerActor->MoveToLocation(vector, 50.0f);// , false, false, false, true, FilterClass, true);
	if (result != EPathFollowingRequestResult::RequestSuccessful) //result is Failed or AlreadyAtGoal
	{
		printMessage(FColor::Green, result == EPathFollowingRequestResult::AlreadyAtGoal ? "AT-GOAL" : "FAILED");
		direction[axis] *= -1;
		//failed = true;
	}
	/*FVector newVector = OwnerActor->GetPawn()->GetActorLocation();
	if (callNumber % 10 == 0)
	{
	printMessage(FColor::Green, FString::FromInt(X) + "," + FString::FromInt(Y) + " ----- " + FString::FromInt(newVector.X) + "," + FString::FromInt(newVector.Y));
	}
	if ((axis == X_AXIS && X == newVector.Component(X_AXIS)) || (axis == Y_AXIS  && Y == newVector.Component(Y_AXIS)))
	{
	if (callNumber % 10 == 0)
	{
	printMessage(FColor::Green, FString("vector unchanged   ") + (axis == X_AXIS ? "X" : "Y"));
	}
	//direction[axis] *= -1;
	failed = true;
	}*/
	//printMessage(FColor::Green, FString::FromInt(X) + "," + FString::FromInt(Y) + "+++++++++++++++++++");
}

/*
* Making steps in the given direction, using the function goDirect.
*/
void makeSteps(AAIController* OwnerActor, FVector &vector)
{
	bool failed = false;
	if (steps > 0)
	{
		steps--;
		goDirect(OwnerActor, vector, goAxis);// , failed);
		goDirect(OwnerActor, vector, goAxis);// , failed);
	}
}

/*
* Opening the orders file, named ORDERS_FILE_NAME.
* It gets the path to the file using the GameDir function.
*/
bool UCustomAiBlueprintBTT::openOrdersFile()
{
	if (ordersFile.is_open())
	{
		return true;
	}
	FString gameDirFstring = FPaths::GameDir();
	string gameDir = TCHAR_TO_UTF8(*gameDirFstring); //converting the FString to string
	gameDir += ORDERS_FILE_NAME;
	ordersFile.open(gameDir);
	if (!ordersFile.is_open())
	{
		return false;
	}
	return true;
}

/*
* walkByScreensaver mode - walk diagonal.
* When bouncing in an element, change direction in the same angle.
*/
void UCustomAiBlueprintBTT::walkByScreensaver(AAIController* OwnerActor)
{
	if (steps > 0)
	{
		steps--;
	}
	else if (steps != STEPS_DONT_CHANGE_MODE)
	{
		operationMode = MODE_ORDERSFILE;
		printMessage(FColor::Green, "changing mode to orders file");
		return;
	}

	FVector vector = OwnerActor->GetPawn()->GetActorLocation();
	//bool failed = false;

	goDirect(OwnerActor, vector, X_AXIS);// , failed);
	//if (!failed)
	//{
	//vector = OwnerActor->GetPawn()->GetActorLocation();
	goDirect(OwnerActor, vector, Y_AXIS);// , failed);
	//}
}

/*
* walkByFileorders mode - follow orders written in file.
* It first checks if the file is open, and opens it otherwise.
* The function checks is the last order is still being executed.
* If so, it calls makeSteps to make another step for the order.
* If not so, it starts the next order, by updating the direction and the direction and the amount of steps.
*/
void UCustomAiBlueprintBTT::walkByFileorders(AAIController* OwnerActor)
{
	FVector vector = OwnerActor->GetPawn()->GetActorLocation();

	if (!openOrdersFile())
	{
		printMessage(FColor::Red, "ERROR: orders file could not be opened");
		return;
	}

	if (steps <= 0)
	{
		int distance = 0;
		char axis = 0;
		string mode;
		if (ordersFile >> mode)
		{
			orderNumber++;
			printMessage(FColor::Yellow, stringToFString(string("Order Number: " + std::to_string(orderNumber))));
			if (mode.at(0) == '#')
			{
				printMessage(FColor::Green, "Comment");
				std::getline(ordersFile, mode);
				return;
			}
			else if (mode == "WALK")
			{
				ordersFile >> distance >> axis;
				printMessage(FColor::Green, stringToFString(std::to_string(distance) + string(" ") + string(1, axis)));
				steps = ABS(distance);
				goAxis = axis == 'X' ? X_AXIS : Y_AXIS;
				direction[goAxis] = distance > 0 ? POSITIVE_DIRECTION : NEGATIVE_DIRECTION;
			}
			else if (mode == "SCREENSAVER")
			{
				ordersFile >> distance;
				steps = ABS(distance);
				operationMode = MODE_SCREENSAVER;
				printMessage(FColor::Green, "Changing mode to screensaver");
				return;
			}
			else if (mode == "GOTO")
			{
				ordersFile >> dest_X >> dest_Y;
				operationMode = MODE_GOTO;
				printMessage(FColor::Green, FString("Going to point ") + FString::FromInt(dest_X) + "," + FString::FromInt(dest_Y));// +"," + FString::FromInt(dest_Z));
				return;
			}
			else if (mode == "FOLLOW")
			{
				ordersFile >> actorNumber;
				operationMode = MODE_FOLLOW;
				printMessage(FColor::Green, FString("Following actor ") + FString::FromInt(actorNumber));
				return;
			}
			else if (mode == "SHOOT")
			{
				printMessage(FColor::Yellow, "shoot comment---------------------------");
				//distance = steps;
				//ordersFile >> steps;
				//ordersFile >> distance;
				//steps = ABS(distance);
				//steps = distance;
				ordersFile >> shootTime;
				FOutputDeviceNull ar;
				bool shootResult = /*true;//*/ OwnerActor->CallFunctionByNameWithArguments(TEXT("shootCPP"), ar, this, true);
				//if (OwnerActor->CallFunctionByNameWithArguments(TEXT("shootCPP"), ar, this, true))
				if (shootResult == false)
				{
					printMessage(FColor::Green, "FAILURE in CallFunctionByNameWithArguments shootCPP");
					shootTime = -5;
					return;
				}
				printMessage(FColor::Yellow, "shoot");
				//operationMode = MODE_SHOOT;
				//std::getline(ordersFile, mode);
				//shootTime = atoi(mode.c_str());
				return;
			}
			else
			{
				printMessage(FColor::Yellow, stringToFString(string("ERROR: wrong operation mode ") + mode));
				std::getline(ordersFile, mode);
				return;
			}
		}
		else
		{
			operationMode = MODE_SCREENSAVER;
			steps = STEPS_DONT_CHANGE_MODE;
			printMessage(FColor::Green, "Finish orders file - mode is screensaver");
		}
	}

	makeSteps(OwnerActor, vector);
}

/*
* walkByEnemies mode - follow enemies.
* Partially implemented.
*/
void walkByEnemies(AAIController* OwnerActor, TArray<AActor*> Enemies, int callNumber)
{
	FVector vector = OwnerActor->GetPawn()->GetActorLocation();
	int i = 0;
	for (AActor* enemy : Enemies)
	{
		i++;
		FVector enemyVector = enemy->GetActorLocation();
		FVector distance;
		distance.X = enemyVector.X - vector.X;
		distance.Y = enemyVector.Y - vector.Y;
		if (callNumber % 10 == 0)
		{
			printMessage(FColor::Green, "Location of enemy: " + FString::FromInt(enemyVector.X) + " : " + FString::FromInt(enemyVector.Y));
			printMessage(FColor::Green, "Distance: " + FString::FromInt(distance.X) + " : " + FString::FromInt(distance.Y));
		}
	}
	if (callNumber % 10 == 0)
	{
		printMessage(FColor::Red, "Enemies number: " + FString::FromInt(i));
	}
}

void UCustomAiBlueprintBTT::shoot(AAIController* OwnerActor)
{
	if (shootTime > 0)
	{
		shootTime--;
		return;
	}
	if (shootTime < 0)
	{
		return;
	}
	shootTime--;
	FOutputDeviceNull ar;
	/*if (OwnerActor->CallFunctionByNameWithArguments(TEXT("unshootCPP"), ar, this, true))
	{
		//printMessage(FColor::Green, "SUCCESS");
	}*/
	bool shootResult = OwnerActor->CallFunctionByNameWithArguments(TEXT("unshootCPP"), ar, this, true);
	if (shootResult == false)
	{
		printMessage(FColor::Green, "FAILURE in CallFunctionByNameWithArguments unshootCPP");
	}
	//operationMode = MODE_ORDERSFILE;
}

/*
* Chooses the mode of operation and operating it: ordersfile or screensaver.
* the mode is chosen according to the operationMode variable.
*/
void UCustomAiBlueprintBTT::chooseOperationMode(AAIController* OwnerActor, TArray<AActor*> Enemies)
{
	if (operationMode == MODE_SCREENSAVER) //Going like the screensaver
	{
		walkByScreensaver(OwnerActor);
	}
	else if (operationMode == MODE_ORDERSFILE) //Going by orders
	{
		this->walkByFileorders(OwnerActor);
	}
	else if (operationMode == MODE_GOTO) //Going to a point
	{
		goToPoint(OwnerActor);
	}
	else if (operationMode == MODE_FOLLOW) //Following enemies
	{
		//walkByEnemies(OwnerActor, Enemies, callNumber);
		followActor(OwnerActor, Enemies);
	}
	else if (operationMode == MODE_SHOOT)
	{
		this->UCustomAiBlueprintBTT::shoot(OwnerActor);
	}
	else
	{
		printMessage(FColor::Red, "ERROR: wrong operation mode");
	}
}

/*void shootTry(AAIController* OwnerActor)
{
	TFieldIterator funcIt(OwnerActor->GetClass(), false);
	while (funcIt)
	{
		UFunction* function = *funcIt;
		OwnerActor->ProcessEvent(function, NULL);
		++funcIt;
	}
}*/

/*bool myCallFunctionByNameWithArguments(const TCHAR* Str, FOutputDevice& Ar, UObject* Executor)
{
	// Find an exec function.
	FString MsgStr;
	FName Message = NAME_None;
	UFunction* Function = NULL;
	if
		(!FParse::Token(Str, MsgStr, true)
		|| (Message = FName(*MsgStr, FNAME_Find)) == NAME_None
		|| (Function = FindFunction(Message)) == NULL
		|| (Function->FunctionFlags & FUNC_Exec) == 0)
	{
		return false;
	}
	return true;
}*/

/*
* The main function, called in an endless loop.
* Calls the function chooseOperationMode to activate the right mode.
*/
void UCustomAiBlueprintBTT::DoStuff(AAIController* OwnerActor, TArray<AActor*> Enemies)
{
	callNumber++;
	if (callNumber % 10 == 0)
	{
		printMessage(FColor::Red, "call " + FString::FromInt(callNumber));
		//goToPoint(OwnerActor);
	}
	/*if (callNumber % 100 == 0)
	{
		FOutputDeviceNull ar;
		if (OwnerActor->CallFunctionByNameWithArguments(TEXT("shootCPP"), ar, this, true))
		{
			//printMessage(FColor::Green, "SUCCESS");
		}
		else
		{
			printMessage(FColor::Green, "FAILURE in CallFunctionByNameWithArguments shootCPP");

		}
	}
	if (callNumber % 100 == 50)
	{
		FOutputDeviceNull ar;
		if (OwnerActor->CallFunctionByNameWithArguments(TEXT("unshootCPP"), ar, this, true))
		{
			//printMessage(FColor::Green, "SUCCESS");
		}
		else
		{
			printMessage(FColor::Green, "FAILURE in CallFunctionByNameWithArguments unshootCPP");

		}
	}*/
	//followActor(OwnerActor, Enemies);
	//if (callNumber != 200) return;
	//FVector vector = OwnerActor->GetPawn()->GetActorLocation();
	//bool failed = false;
	//goDirect(OwnerActor, vector, X_AXIS, failed, Enemies);
	//goDirect(OwnerActor, vector, X_AXIS, failed);// , Enemies);
	//goToPoint(OwnerActor, vector);

	shoot(OwnerActor);
	chooseOperationMode(OwnerActor, Enemies);
}