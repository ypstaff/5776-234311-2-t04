// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "ArrayMap.h"

void UArrayMap::Add(int32 key, UObject* element) {
	if (!this->arrayMap.Contains(key)) {
		this->arrayMap.Add(key, TArray<UObject*>());
	}
	this->arrayMap[key].Add(element);
}

void UArrayMap::SetArray(int32 key, TArray<UObject*> arr) {
	this->arrayMap.Remove(key);
	this->arrayMap.Add(key, arr);
}


TArray<UObject*>& UArrayMap::GetArray(int32 key) {
	return this->arrayMap.FindOrAdd(key);
}

void UArrayMap::Clear(int key) {
	this->arrayMap.Remove(key);
}