// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "HashMap.h"



UObject* UHashMap::Get(FString key) {
	return this->map.FindOrAdd(key);
}

void UHashMap::Set(FString key, UObject* newValue) {
	this->map.Add(key, newValue);
}

void UHashMap::Remove(FString key) {
	this->map.Remove(key);
}

void UHashMap::Clear() {
	this->map.Reset();
}

TArray<FString> UHashMap::GetKeys() {
	TArray<FString> retVal;
	this->map.GenerateKeyArray(retVal);
	return retVal;
}

bool UHashMap::Contains(FString key) {
	return this->map.Contains(key);
}