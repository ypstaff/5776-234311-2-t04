// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "PlayerData.h"



int32 UPlayerData::GetPlayerScore() {
	return this->PlayerScore;
}

FString UPlayerData::GetPlayerName() {
	return this->PlayerName;
}

int32 UPlayerData::GetScoreboardRowIndex() {
	return this->ScoreboardRowIndex;
}


void UPlayerData::SetPlayerScore(int32 score) {
	this->PlayerScore = score;
}

void UPlayerData::SetScoreboardRowIndex(int32 rowIndex){
	this->ScoreboardRowIndex = rowIndex;
}

void UPlayerData::SetPlayerName(FString name) {
	this->PlayerName = name;
}

void UPlayerData::ConfigurePlayerData(FString name, int32 score, int32 rowIndex) {
	this->SetPlayerName(name);
	this->SetPlayerScore(score);
	this->SetScoreboardRowIndex(rowIndex);
}
