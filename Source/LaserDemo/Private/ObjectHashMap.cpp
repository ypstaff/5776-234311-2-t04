// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserDemo.h"
#include "ObjectHashMap.h"



void UObjectHashMap::Add(UObject* key, float element) {
	if (!this->objectHashMap.Contains(key)) {
		this->objectHashMap.Add(key, TArray<float>());
	}
	this->objectHashMap[key].Add(element);
}

void UObjectHashMap::SetArray(UObject* key, TArray<float> arr) {
	this->objectHashMap.Remove(key);
	this->objectHashMap.Add(key, arr);
}


TArray<float>& UObjectHashMap::GetArray(UObject* key) {
	return this->objectHashMap.FindOrAdd(key);
}

void UObjectHashMap::Clear(UObject* key) {
	this->objectHashMap.Remove(key);
}

void UObjectHashMap::Empty() {
	this->objectHashMap.Empty();
}
